const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type: String,
		//Requires the data to be included when creating a record
		// The "true" value defines if the field is required or not and the second element in the array is the message that will be printed or displayed in terminal when the data is not present
		required: [true, "Course is required"]
	},
	description:{
		type: String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		// the "new Date()" expression instantiate a new "date" that the current data and time whenever a course is created in our database
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "UserID is required"]
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			}
		}
	]
	
})

								//Model Name
module.exports = mongoose.model("Course", courseSchema);