// dependencies
const express = require("express");
const router = express.Router();
const User = require("../Models/user.js");
const userController = require("../Controllers/userController.js");
const auth = require("../auth.js");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/details", (req, res) => {
	userController.userDetails(req.body).then(result => res.send(result));
});

//specific document
router.get("/details/:id", (req, res) => {
	userController.userDetailsA(req.params.id).then(result => res.send(result));
});

//enroll
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(result => res.send(result));
});

module.exports = router;