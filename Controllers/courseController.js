const mongoose = require("mongoose");
const Course = require("../Models/course.js");

module.exports.addCourse = (data) => {
	console.log(data.isAdmin)

	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});

		return newCourse.save().then((newCourse, error) => {
			if(error){
				return error;
			}
				return newCourse
		})
	};

		let message = Promise.resolve('User must be ADMIN to access this')
			return message.then((value) => {
				{return value}
			})
	
};

module.exports.getAllCourse = () => {
					// {} document
	return Course.find({}).then(result => {
		return result;
	})
}

// GET all active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	})
}

// GET specific course
module.exports.getCourse = (courseId) => {
						// inside the parenthesis should be the id
	return Course.findById(courseId).then(result => {
		return result;
	})
}


// Updating a course
										// will contain mult fields
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		//update code
		return Course.findByIdAndUpdate(courseId, {
			name: newData.course.name,
			description: newData.course.description,
			price: newData.course.price
		}).then((updatedCourse, error) => {
			if(error){
				return false
			}
				return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
			return message.then((value) => {return value})
	}
}

module.exports.archiveCourse = (courseId) => {
	if(courseId.isAdmin == true){
	return Course.findByIdAndUpdate(courseId, {
		isActive: false
	})
	.then((archiveCourse, error) => {
		if(error){
			return false
		}
			return{
				message: "Course archived successfully!"
			}
	})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
			return message.then((value) => {return value})
	}
};