/*
 gitBash:
 	npm init -y
 	npm install express
 	npm install mongoose
 	npm install cors
 	npm install bcrypt
 	npm install jsonwebtoken
*/


// dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//routers
const userRoute = require("./Routes/userRoute.js");
const courseRoutes = require("./Routes/courseRoutes.js");

// to create an express server/application
const app = express();

//Middlewares - allows to bridge our backend application (server) to our frontend
//to allow cross origin resourse sharing
app.use(cors());
//to read json objects
app.use(express.json());
//to read forms
app.use(express.urlencoded({extended:true}));


// initializing the routes
app.use("/users", userRoute);
app.use("/courses", courseRoutes);

//connect to our mongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.38t9udg.mongodb.net/courseBooking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// prompts a message once connected
mongoose.connection.once('open', () => console.log('Now connected to Benigay-MongoDB Atlas.'));


//prompts a mesage once connected to port 4000
app.listen(process.env.PORT || 4000, () =>
	{console.log(`API is now online on port ${process.env.PORT || 4000}`)});